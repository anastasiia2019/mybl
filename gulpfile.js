const gulp = require('gulp'),
      cssmin = require('gulp-cssmin'),
      rename = require('gulp-rename'),
      minify = require('gulp-minify'),
      pug = require('gulp-pug'),
      sass = require('gulp-sass'),
      autoprefixer = require('gulp-autoprefixer'),
      concatCss = require('gulp-concat-css'),
      browserSync = require('browser-sync').create(),
      babel = require('gulp-babel'),
      concat = require('gulp-concat');
      sass.compiler = require('node-sass');


gulp.task('server', function() {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
    browserSync.watch('./build', browserSync.reload);
});

gulp.task('copy-fonts', function() {
  return (
    gulp.src( './src/fonts/**/*.*')
    .pipe(gulp.dest('build/fonts'))
  )
});

gulp.task('copy-img', function() {
  return (
    gulp.src('./src/img/**/*.*',)
    .pipe(gulp.dest('build/img'))
  )
});

gulp.task('compress', function() {
  return (
    gulp.src(['./src/**/*.js', '!src/js/library/*.js'])
    .pipe(babel({
            presets: ['@babel/env'],
            plugins: [["@babel/plugin-proposal-class-properties", { "loose": true }]]
      }))
      .pipe(concat('main.js'))
      .pipe(minify())
      .pipe(rename({ suffix: '.min' }))
      .pipe(gulp.dest('build/js'))
  )
});
gulp.task('copy-js-lib', function() {
  return (
    gulp.src('./src/js/library/*.*')
    .pipe(gulp.dest('build/js'))
  )
});

gulp.task('css', function () {
    return (
        gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 version'))
        .pipe(concatCss("style.css", {rebaseUrls: false}))
        .pipe(cssmin({}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('build/css'))
      )
});

gulp.task('pug', function buildHTML() {
  return gulp.src('./src/pug/**/*.pug')
  .pipe(pug({
    pretty: true
  }))
  .pipe(gulp.dest('./build'))
  .pipe(gulp.dest('./build/'))
});

gulp.task('watch', function() {
  gulp.watch('./src/img/**/*.*', gulp.series('copy-img'));
  gulp.watch('./src/fonts/*/*', gulp.series('copy-fonts'));
  gulp.watch('./src/pug/**/*.pug', gulp.series('pug'));
  gulp.watch('./src/**/*.js', gulp.series('compress'));
  gulp.watch('./src/**/*.scss', gulp.series('css'));
  gulp.watch('./src/main.css', gulp.series('css'));
  gulp.watch('./src/js/library/*.*', gulp.series('copy-js-lib'));
});

gulp.task('default', gulp.series(
  'pug', 'compress', 'css','copy-img', 'copy-fonts', 'copy-js-lib',
  gulp.parallel('watch', 'server'),
));

gulp.task('production',function () {
    return (
        gulp.src(['./index.js','./package.json','./build/**/*.*'])
        .pipe(gulp.dest('production/'))
      )
});
