var express = require("express");

var app = express();
const path = require('path');
const router = express.Router();

// app.use(express.static('templates'));

router.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/index.html'));
});

router.get('/products.html',function(req,res){
    res.sendFile(path.join(__dirname+'/products.html'));
});
router.get('/about.html',function(req,res){
    res.sendFile(path.join(__dirname+'/about.html'));
});
router.get('/vacancies.html',function(req,res){
    res.sendFile(path.join(__dirname+'/vacancies.html'));
});
router.get('/vacancy.html',function(req,res){
    res.sendFile(path.join(__dirname+'/vacancy.html'));
});
router.get('/erp-cloud.html',function(req,res){
    res.sendFile(path.join(__dirname+'/erp-cloud.html'));
});
router.get('/portfolio.html',function(req,res){
    res.sendFile(path.join(__dirname+'/portfolio.html'));
});

app.use('/', router);
app.use('/css', express.static(__dirname + '/css'));
app.use('/js', express.static(__dirname + '/js'));
app.use('/img', express.static(__dirname + '/img'));
app.use('/fonts', express.static(__dirname + '/fonts'));

var port = process.env.PORT || 8080;

var server = app.listen(port, function(){
    var port = server.address().port;
    console.log("Server started at http://localhost:%s", port);
});
