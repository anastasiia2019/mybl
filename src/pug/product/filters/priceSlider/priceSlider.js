const inputs = document.querySelectorAll('.input__slider');
const greyLeft = document.querySelector('.greyLeft');
const thumbLeft = document.querySelector('.thumbLeft');
const range = document.querySelector('.range');
const valueLeftText = document.querySelector('.valueLeftText');

const greyRight = document.querySelector('.greyRight');
const thumbRight = document.querySelector('.thumbRight');
const valueRightText = document.querySelector('.valueRightText');


if(inputs.length > 0 ){
  inputs.forEach((input) =>{
    input.addEventListener('input', e => {
      let target = e.target;
      if (target.hasAttribute('data-min')){
        target.value = Math.min(target.value, inputs[1].value-1);
        var value=(100/(parseInt(target.max)-parseInt(target.min)))*parseInt(target.value)-(100/(parseInt(target.max)-parseInt(target.min)))*parseInt(target.min);

        greyLeft.width=value+'%';
        range.style.left=value+'%';
        thumbLeft.style.left=value+'%';
        valueLeftText.innerHTML=target.value;
      }else{
        target.value = Math.max(target.value, inputs[0].value-1);
        var value=(100/(parseInt(target.max)-parseInt(target.min)))*parseInt(target.value)-(100/(parseInt(target.max)-parseInt(target.min)))*parseInt(target.min);

        greyRight.width=(100-value)+'%';
        range.style.right=(100-value)+'%';
        thumbRight.style.left=value+'%';
        valueRightText.innerHTML=target.value + "грн";
      }
  }, false);
  })
}
