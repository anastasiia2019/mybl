if(document.querySelector(".categories__slider")){
  const categoriesSlider = tns({
      container: '.categories__slider',
      controls: false,
      items: 5.7,
      mouseDrag: true,
      autoplay: true,
      autoplayButtonOutput: false,
      nav: false,
  });
}
