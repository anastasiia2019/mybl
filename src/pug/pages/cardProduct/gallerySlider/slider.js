if(document.querySelector(".tns-gallery")){
  const slider = tns({
      container: '.tns-gallery',
      controlsContainer: '.galleryControls',
      items: 1,
      nav: false,
      loop: false
  });
}
